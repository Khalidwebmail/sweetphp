<?php
  // Load Libraries
 
  include_once 'config/config.php';
  
  function autoloadClass( $class )
  {
      require_once 'libraries/' . $class . '.php';
  }
  spl_autoload_register('autoloadClass');
  
  
  