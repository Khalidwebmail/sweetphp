<?php

/**
 * Default settings
 */

define( 'APPROOT', dirname( dirname( __FILE__ ) ) );

define( 'URLROOT', 'http://localhost/sweetphp' );

define( 'SITENAME', 'SweetPHP' );

/**
 * Default host
 */
define( 'DB_HOST', 'localhost' );

/**
 * Default database user
 */
define( 'DB_USER', 'root' );

/**
 * Default database password
 */
define( 'DB_PASS', '' );

/**
 * Defualt database name
 */
define( 'DB_NAME', '' );