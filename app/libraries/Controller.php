<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Controller
 *
 * @author aa_kh
 */
class Controller {
    
    /**
     * Load model dynamically
     * @param type $model
     * @return object $model
     */
    public function loadModel( $model ) 
    {
        require_once '../app/models/' . $model . '.php';
        return new $model;
    }
    
    /**
     * Load html view dynamically 
     * @param string $view
     * @param array $data
     */
    public function loadView( $view, $data = [] ) 
    {
        if( file_exists( '../app/views/' . $view . '.php' ) )
        {
            include_once '../app/views/' . $view . '.php';
        }
        else{
            include_once '../app/views/404.php';
        }
    }
}
