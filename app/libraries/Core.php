<?php
  /*
   * App Core Class
   * Creates URL & loads core controller
   * URL FORMAT - /controller/method/params
   */
  class Core {
    protected $controller = 'Home';
    protected $method = 'index';
    protected $params = [];

    public function __construct(){
      //print_r($this->getUrl());

      $url = $this->getUrl();

      // Look in controllers for first value
      if( ! empty( $url[0] ) && file_exists('../app/controllers/' . ucwords( $url[0] ). '.php' ) ){
        // If exists, set as controller
        $this->controller = ucwords($url[0]);
        // Unset 0 Index
        unset( $url[0] );
      }

      // Require the controller
      require_once '../app/controllers/'. $this->controller . '.php';

      // Instantiate controller class
      $this->controller = new $this->controller();

      // Check for second part of url
      if( ! empty( $url[1] ) ){
        // Check to see if method exists in controller
        if( method_exists($this->controller, $url[1] ) ){
          $this->method = $url[1];
          // Unset 1 index
          unset( $url[1] );
        }
      }

      // Get params
      $this->params = $url ? array_values($url) : [];
      // Call a callback with array of params
      call_user_func_array( [ $this->controller, $this->method ], $this->params );
    }
    
    /**
     * Get current URL
     * @return mixed
     */
    public function getUrl(){
      if( !empty( $_GET['url'] ) ){
        $url = rtrim( $_GET['url'], '/' );
        $url = filter_var( $url, FILTER_SANITIZE_URL );
        $url = explode( '/', $url );
        return $url;
      }
    }
  } 
  
  