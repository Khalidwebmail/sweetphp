<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CsrfToken
 *
 * @author aa_kh
 */
session_start();

class CsrfToken {
    /**
     * Generate unique token
     * @return string
     */
    public static function generateToken() {
        $_SESSION['token'] = sha1(uniqid());
        return $_SESSION['token'];
    }
    
    /**
     * Check getting token is and session token same or not
     * @param string $token
     * @return boolean
     */
    public static function checkToken($token) {
        if( !empty($token) && $token === $_SESSION['token'] ) {
            unset($_SESSION['token']);
            return true;
        }
        else{
            return false;
        }
    }
}
