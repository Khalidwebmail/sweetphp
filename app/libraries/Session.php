<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Session
 *
 * @author aa_kh
 */
session_start();

class Session {
    /**
     * Store value in session
     * @param string $name
     * @param $value
     * @return type
     */
    public static function put($name = null, $value = null) {
        $_SESSION[$name] = $value;
        return $_SESSION[$name];
    }
    
    /**
     * Check session any value exists or not
     * @param string $name
     * @return boolean
     */
    public static function exists($name = null) {
        if( !empty( $name ) && $_SESSION[$name] !== null ) {
            return true;
        }
        return false;
    }
    
    /**
     * Get value from session
     * @param string $name
     * @return any
     */
    public static function get($name) {
        return $_SESSION[$name];
    }
    
    /**
     * Delete value from session
     * @param string $name
     */
    public static function flush($name) {
        if( self::exists( $name ) ) {
            unset( $_SESSION[$name] );
        }
    }
}